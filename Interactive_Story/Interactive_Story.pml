<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Interactive_Story" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="0_0_interactive_start" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="1_1_have_been_sick_yes" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="1_1_have_been_sick_no" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="2_1_bacteria_know_yes" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="2_1_bacteria_know_no" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="3_how_defense_yes" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="3_how_defense_no_or_default" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="5_enough_yes" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="5_enough_no" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="0_3_repeat_been_sick" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="0_2_been_sick_before" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="0_1_repeat_name" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="9_understand" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="1_0_bacteria_question" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="1_1_have_been_sick_yes_interested" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="2_1_bacteria_know_yes_interested" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="2_0_defense_question" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="4_1_multiply_wrong" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="4_1_multiply_correct" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="4_0_whats_next" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="0_2b_been_sick_no_name" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="3_0_how_defense_repeat_question" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="choice_sentences" src="0_0_interactive_start/Aldebaran/choice_sentences.xml" />
        <File name="_metadata" src="_metadata" />
    </Resources>
    <Topics />
    <IgnoredPaths />
</Package>
